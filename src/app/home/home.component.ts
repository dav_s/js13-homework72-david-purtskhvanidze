import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Dish } from '../shared/dish.model';
import { DishService } from '../shared/dish.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit, OnDestroy {
  dishes: Dish[] = [];
  dishesChangeSubscription!: Subscription;
  dishesFetchingSubscription!: Subscription;
  isFetching = false;

  constructor(private dishService: DishService) { }

  ngOnInit(): void {
    this.dishes = this.dishService.getDishes();
    this.dishesChangeSubscription = this.dishService.dishChange.subscribe((dishes: Dish[]) => {
      this.dishes = dishes;
    });
    this.dishesFetchingSubscription = this.dishService.dishFetching.subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
    });
    this.dishService.fetchDishes();
  }

  removeDish(id: string) {
    this.dishService.removeDish(id).subscribe(() => {
      this.dishService.fetchDishes();
    });
  }

  ngOnDestroy() {
    this.dishesChangeSubscription.unsubscribe();
    this.dishesFetchingSubscription.unsubscribe();
  }

}
