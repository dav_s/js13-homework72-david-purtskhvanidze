export class Step {
  constructor(
    public stepDescription: string,
    public stepImg: string,
  ) {}
}
