export class Dish {
  constructor(
    public id: string,
    public name: string,
    public description: string,
    public img: string,
    public ingredients: string,
    public steps: [],
  ) {}
}
