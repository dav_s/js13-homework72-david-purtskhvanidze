import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Dish } from './dish.model';
import { HttpClient } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';

@Injectable()
export class DishService {
  dishChange = new Subject<Dish[]>();
  dishFetching = new Subject<boolean>();
  dishUploading = new Subject<boolean>();
  dishRemoving = new Subject<boolean>();

  private dishes: Dish[] = [];

  constructor(private http: HttpClient) {}

  getDishes() {
    return this.dishes.slice();
  }

  fetchDishes() {
    this.dishFetching.next(true);
    this.http.get<{ [id: string]: Dish}>('https://rest-b290b-default-rtdb.firebaseio.com/dishes.json')
      .pipe(map(result => {
        if (result === null) {
          return [];
        }

        return Object.keys(result).map(id => {
          const dishData = result[id];
          return new Dish(
            id,
            dishData.name,
            dishData.description,
            dishData.img,
            dishData.ingredients,
            dishData.steps,
          );
        });
      }))
      .subscribe(dishes => {
        this.dishes = dishes;
        this.dishChange.next(this.dishes.slice());
        this.dishFetching.next(false);
      }, () => {
        this.dishFetching.next(false);
      });
  }

  fetchDish(id: string) {
    return this.http.get<Dish | null>(`https://rest-b290b-default-rtdb.firebaseio.com/dishes/${id}.json`)
      .pipe(
        map(result => {
          if (!result) {
            return null;
          }
          return new Dish(id, result.name, result.description, result.img, result.ingredients, result.steps);
        }),
      );
  }

  addDish(dish: Dish) {
    const body = {
      name: dish.name,
      description: dish.description,
      img: dish.img,
      ingredients: dish.ingredients,
      steps: dish.steps,
    };

    this.dishUploading.next(true);

    return this.http.post('https://rest-b290b-default-rtdb.firebaseio.com/dishes.json', body).pipe(
      tap(() => {
        this.dishUploading.next(false);
      }, () => {
        this.dishUploading.next(false);
      })
    );
  }

  removeDish(id: string) {
    this.dishRemoving.next(true);

    return this.http.delete(`https://rest-b290b-default-rtdb.firebaseio.com/dishes/${id}.json`).pipe(
      tap(() => {
        this.dishRemoving.next(false);
      }, () => {
        this.dishRemoving.next(false);
      })
    );
  }

}
