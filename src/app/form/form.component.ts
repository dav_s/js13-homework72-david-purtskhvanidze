import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { DishService } from '../shared/dish.service';
import { Dish } from '../shared/dish.model';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  addForm!: FormGroup;

  constructor(
    private dishService: DishService,
    private http: HttpClient,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.addForm = new FormGroup({
      'dishName': new FormControl(null, Validators.required),
      'dishDescription': new FormControl(null, Validators.required),
      'dishImg': new FormControl(null, Validators.required),
      'dishIngredients': new FormControl(null, [Validators.required]),
      'steps': new FormArray([])
    });
  }

  onSubmit() {
    const id = Math.random().toString();
    const dish = new Dish(
      id,
      this.addForm.value.dishName,
      this.addForm.value.dishDescription,
      this.addForm.value.dishImg,
      this.addForm.value.dishIngredients,
      this.addForm.value.steps,
    );

    const next = () => {
      this.dishService.fetchDishes();
      void this.router.navigate(['/'], {relativeTo: this.route});
    };

    this.dishService.addDish(dish).subscribe(next);
  }

  fieldHasError(fieldName: string, errorType: string) {
    const field = this.addForm.get(fieldName);
    return field && field.touched && field.errors?.[errorType];
  }

  addStep() {
    const steps = <FormArray>this.addForm.get('steps');
    const stepsGroup = new FormGroup({
      stepImg: new FormControl(null, Validators.required),
      stepDescription: new FormControl(null, Validators.required),
    });
    steps.push(stepsGroup);
  }

  getStepsControls() {
    return (<FormArray>this.addForm.get('steps')).controls;
  }

  getFormValid() {
    return this.addForm.valid;
  }
}
