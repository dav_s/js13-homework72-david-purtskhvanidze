import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Data, Router } from '@angular/router';
import { DishService } from '../shared/dish.service';
import { Dish } from '../shared/dish.model';
import { Step } from '../shared/step.model';

@Component({
  selector: 'app-dish',
  templateUrl: './dish.component.html',
  styleUrls: ['./dish.component.css']
})

export class DishComponent implements OnInit {
  dish!: Dish;
  steps: Step[] = [];

  isFetching = false;

  constructor(
    private dishService: DishService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }


  ngOnInit(): void {
    this.isFetching = true;
    this.route.data.subscribe((data: Data) => {
      this.dish = <Dish>data.dish;
      this.steps = this.dish.steps;
      this.isFetching = false;
    });
  }

}
